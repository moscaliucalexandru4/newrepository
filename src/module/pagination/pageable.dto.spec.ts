import { Pageable } from './pageable.dto';

describe('Pageable', () => {
  it('It should get correct value when inputs are positive numbers ', () => {
    const result = new Pageable(5, 10);
    expect(result.getSkip()).toBe(40);
  });

  it('It should get correct value when inputs are negative numbers , defaults settings', () => {
    const result = new Pageable(-1, -10);
    expect(result.getSkip()).toBe(0);
  });

  it('It should get correct value when page are negative numbers , defaults settings', () => {
    const result = new Pageable(-1, 10);
    expect(result.getSkip()).toBe(0);
  });

  it('It should get correct value when limit is negative numbers , defaults settings', () => {
    const result = new Pageable(1, -10);
    expect(result.getSkip()).toBe(0);
  });
});
