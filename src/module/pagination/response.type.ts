export interface PageResponse<T> {
  data: T,
  limit: number,
  page: number,
  total: number
}
