import { Transform } from 'class-transformer';
import { IsOptional } from 'class-validator';

export class Pageable {
  constructor(
    page = 1,
    limit = 10,
  ) {
    this.page = page;
    this.limit = limit;
  }

  @Transform(({ value }) => {
    return Math.max(Number(value), 1);
  })
  @IsOptional()
  public page = 1;

  @Transform(({ value }) => {
    return Number(value) > 0 ? Number(value) : 10;
  })
  @IsOptional()
  public limit = 10;

  getSkip(): number {
    return this.page > 0 && this.limit > 0
      ? (this.page - 1) * this.limit
      : 0;
  }
}
