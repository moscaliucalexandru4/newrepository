import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('employee')
export class EmployeeEntity {
  constructor(
    id:number,
    firstName:string,
    lastName:string,
    email:string,
    createdAt:Date,
    updatedAt:Date,
  ) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  @PrimaryGeneratedColumn()
    id:number;

  @Column({ name: 'first_name' })
    firstName:string;

  @Column({ name: 'last_name' })
    lastName:string;

  @Column()
    email:string;

  @CreateDateColumn()
    createdAt:Date;

  @UpdateDateColumn()
    updatedAt:Date;
}
