import {
  Body, Controller, Delete, Get, HttpCode, Param, Patch, Post, Query,
} from '@nestjs/common';
import {
  ApiBody,
  ApiOkResponse, ApiOperation, ApiResponse, ApiTags,
} from '@nestjs/swagger';

import { Pageable } from '../pagination/pageable.dto';

import { CreateEmployeeDto } from './dto/create-employee.dto';
import { EmployeeDto } from './dto/employee.dto';
import { UpdateEmoloyeeDto } from './dto/update-emoloyee.dto';
import { EmployeeService } from './employee.service';

import type { EmployeeEntity } from './employee-entity/employee.entity';
import type { PageResponse } from '../pagination/response.type';
import type { UpdateResult } from 'typeorm';

@ApiTags('employee')
@Controller('employee')
export class EmployeeController {
  constructor(
    private readonly employeeService:EmployeeService,
  ) {}

  @Get()
  @ApiOperation({ summary: 'Endpoint to get all employees' })
  @ApiOkResponse({
    description: 'Successful get all employees',
    type: [EmployeeDto],
  })
  @ApiResponse({ status: 500, description: 'Failed to get all employee.' })
  async findAll(
    @Query() filter: Pageable,
  ):Promise<PageResponse<EmployeeEntity[]>> {
    return this.employeeService.findAll(filter);
  }

  @Get(':id')
  @ApiOperation({ summary: 'Endpoint to get employee' })
  @ApiOkResponse({
    description: 'Successful get employee',
    type: EmployeeDto,
  })
  @ApiResponse({ status: 500, description: 'Failed to get employee.' })
  async findOne(@Param('id')id:number):Promise<EmployeeEntity | null> {
    return this.employeeService.findOne(id);
  }

  @Post()
  @ApiOperation({ summary: 'Endpoint to post employee' })
  @ApiOkResponse({
    description: 'Successful created employee',
    type: EmployeeDto,
  })
  @ApiResponse({ status: 500, description: 'Failed to post employee.' })
  @ApiBody({ type: CreateEmployeeDto })
  async create(@Body()input:CreateEmployeeDto):Promise<EmployeeEntity> {
    return this.employeeService.create(input);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Endpoint to update employee' })
  @ApiOkResponse({
    description: 'Successful updated employee',
    type: EmployeeDto,
  })
  @ApiResponse({ status: 500, description: 'Failed to update employee.' })
  async update(@Param('id')id:number, @Body()input:UpdateEmoloyeeDto):Promise<UpdateResult> {
    return this.employeeService.update(id, input);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Endpoint to delete employee' })
  @ApiOkResponse({
    description: 'Successful deleted employee',
    type: EmployeeDto,
  })
  @ApiResponse({ status: 500, description: 'Failed to delete employee.' })
  @HttpCode(204)
  async delete(@Param('id') id:number):Promise<void> {
    await this.employeeService.delete(id);
  }
}
