import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { NotFound } from '../errors/not-found-exception';

import { EmployeeEntity } from './employee-entity/employee.entity';

import type { CreateEmployeeDto } from './dto/create-employee.dto';
import type { UpdateEmoloyeeDto } from './dto/update-emoloyee.dto';
import type { Pageable } from '../pagination/pageable.dto';
import type { PageResponse } from '../pagination/response.type';
import type { DeleteResult, UpdateResult } from 'typeorm';

@Injectable()
export class EmployeeService {
  private readonly logger = new Logger(EmployeeService.name);

  constructor(
    @InjectRepository(EmployeeEntity)
    private readonly repository: Repository<EmployeeEntity>,
  ) {
  }

  async findAll(filter: Pageable)
    : Promise<PageResponse<EmployeeEntity[]>> {
    const {
      page,
      limit,
    } = filter;
    const [data, total] = await this.repository.findAndCount({
      skip: filter.getSkip(),
      take: limit,
    });
    return {
      data,
      limit,
      page,
      total,
    };
  }

  async findOne(id: number): Promise<EmployeeEntity | null> {
    const employee = await this.repository.findOneBy({ id });

    if (!employee) {
      throw new NotFound();
    }
    return employee;
  }

  async create(input: CreateEmployeeDto): Promise<EmployeeEntity> {
    this.logger.log('Hit the employee create router ');
    return this.repository.save({
      ...input,
    });
  }

  async update(id: number, input: UpdateEmoloyeeDto): Promise<UpdateResult> {
    const employee: EmployeeEntity | null = await this.repository.findOneBy({ id });

    if (!employee) {
      throw new NotFound();
    }
    return this.repository.update(
      employee,
      input,
    );
  }

  async delete(id: number): Promise<DeleteResult> {
    const employee: EmployeeEntity | null = await this.repository.findOneBy({ id });

    if (!employee) {
      throw new NotFound();
    }
    return this.repository.delete(employee);
  }
}
