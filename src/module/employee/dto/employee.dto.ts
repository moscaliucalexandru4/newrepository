import { ApiProperty } from '@nestjs/swagger';

export class EmployeeDto {
  @ApiProperty()
    id!:number;

  @ApiProperty({ example: 'James' })
    firstName!:string;

  @ApiProperty({ example: 'Smith' })
    lastName!:string;

  @ApiProperty({ example: 'jamesmith@gmail.com' })
    email!:string;
}
