import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class CreateEmployeeDto {
  @ApiProperty({ example: 'James', description: 'Employee first name', type: String })
  @IsNotEmpty()
    firstName!:string;

  @ApiProperty({ example: 'Smith', description: 'Employee last name', type: String })
  @IsNotEmpty()
    lastName!:string;

  @ApiProperty({ example: 'jamessmith@gmail.com', description: 'Employee email', type: String })
  @IsEmail()
    email!:string;
}
