import { NotFoundException } from '@nestjs/common';

export class NotFound extends NotFoundException {
  constructor() {
    super('Could not find the driver');
  }
}
