import { HttpException, HttpStatus } from '@nestjs/common';

export class NotFoundExcep extends HttpException {
  constructor(id:number) {
    super(`Car with  id ${id} not found.`, HttpStatus.FORBIDDEN);
  }
}
