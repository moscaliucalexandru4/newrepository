import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { NotFoundExcep } from '../errors/not-found-exception-car';

import { Car } from './car-entity/car-entity';

import type { CreateCarDto } from './dto/create-car.dto';
import type { UpdateCarDto } from './dto/update-car.dto';
import type { Pageable } from '../pagination/pageable.dto';
import type { PageResponse } from '../pagination/response.type';
import type { UpdateResult } from 'typeorm';

@Injectable()
export class CarService {
  constructor(
    @InjectRepository(Car)
    private readonly carRepository: Repository<Car>,
  ) {
  }

  async findAll(filter: Pageable): Promise<PageResponse<Car[]>> {
    const {
      page,
      limit,
    } = filter;
    const [data, total] = await this.carRepository.findAndCount({
      skip: filter.getSkip(),
      take: limit,
    });
    return {
      data,
      limit,
      page,
      total,
    };
  }

  async findOne(id: number): Promise<Car | null> {
    const car: Car | null = await this.carRepository.findOneBy({ id });
    if (!car) {
      throw new NotFoundException('Not found', {
        cause: new Error(),
        description: `Car with id ${id} not found.`,
      });
    }
    return car;
  }

  async create(BodyInput: CreateCarDto): Promise<Car> {
    return this.carRepository.save({ ...BodyInput });
  }

  async update(id: number, BodyInput: UpdateCarDto): Promise<UpdateResult> {
    const car: Car | null = await this.carRepository.findOneBy({ id });
    if (!car) {
      throw new NotFoundException('Not found', {
        cause: new Error(),
        description: `Car with id ${id} not found.`,
      });
    }

    return this.carRepository.update(id, BodyInput);
  }

  async remove(id: number): Promise<void> {
    const carOne: Car | null = await this.carRepository.findOneBy({ id });
    if (!carOne) {
      throw new NotFoundExcep(id);
    }
    await this.carRepository.delete(id);
  }
}
