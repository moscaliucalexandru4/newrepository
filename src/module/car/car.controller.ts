import {
  Body,
  Controller, Delete, Get, HttpCode, Param, Patch, Post, Query,
} from '@nestjs/common';
import {
  ApiBody,
  ApiOkResponse, ApiOperation, ApiResponse, ApiTags,
} from '@nestjs/swagger';

import { Pageable } from '../pagination/pageable.dto';

import { CarService } from './car.service';
import { CarDto } from './dto/car.dto';
import { CreateCarDto } from './dto/create-car.dto';
import { UpdateCarDto } from './dto/update-car.dto';

import type { Car } from './car-entity/car-entity';
import type { PageResponse } from '../pagination/response.type';
import type { UpdateResult } from 'typeorm';

@ApiTags('car')
@Controller('car')
export class CarController {
  constructor(private readonly carService: CarService) {}

  @Get()
  @ApiOperation({ summary: 'Endpoint to get all cars' })
  @ApiOkResponse({
    description: 'Successful get all cars',
    type: [CarDto],
  })
  @ApiResponse({ status: 500, description: 'Failed to get all cars.' })
  async findAll(
    @Query() filter: Pageable,
  ):Promise<PageResponse<Car[]>> {
    return this.carService.findAll(filter);
  }

  @Get(':id')
  @ApiOperation({ summary: 'Endpoint to get car' })
  @ApiOkResponse({
    description: 'Successful get car',
    type: CarDto,
  })
  @ApiResponse({ status: 500, description: 'Failed to get car.' })
  async findOne(@Param('id') id:number):Promise<Car | null> {
    return this.carService.findOne(id);
  }

  @Post()
  @ApiOperation({ summary: 'Endpoint to create car' })
  @ApiOkResponse({
    description: 'Successful create car',
    type: CarDto,
  })
  @ApiResponse({ status: 500, description: 'Failed to create car.' })
  @ApiBody({ type: CreateCarDto })
  async create(@Body() BodyInput:CreateCarDto):Promise<Car> {
    return this.carService.create(BodyInput);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Endpoint to update car' })
  @ApiOkResponse({
    description: 'Successful update car',
    type: CarDto,
  })
  @ApiResponse({ status: 500, description: 'Failed to update car.' })
  @ApiBody({ type: CreateCarDto })
  async update(@Param('id') id:number, @Body() BodyInput:UpdateCarDto):Promise<UpdateResult> {
    return this.carService.update(id, BodyInput);
  }

  @HttpCode(204)
  @Delete(':id')
  @ApiOperation({ summary: 'Endpoint to delete car' })
  @ApiOkResponse({
    description: 'Successful delete  car',
    type: CarDto,
  })
  @ApiResponse({ status: 500, description: 'Failed to delete car.' })
  async remove(@Param('id') id:number):Promise<void> {
    await this.carService.remove(id);
  }
}
