import {
  Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn,
} from 'typeorm';

@Entity('car')
export class Car {
  constructor(
    id:number,
    brand:string,
    model:string,
    year:number,
    createdAt:Date,
    updatedAt:Date,
  ) {
    this.id = id;
    this.brand = brand;
    this.model = model;
    this.year = year;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  @PrimaryGeneratedColumn({ name: 'id' })
    id: number;

  @Column({ name: 'brand', type: 'varchar', length: 33 })
    brand: string;

  @Column({ name: 'model', type: 'varchar', length: 33 })
    model: string;

  @Column()
    year: number;

  @CreateDateColumn({ name: 'create_at' })
    createdAt: Date;

  @UpdateDateColumn({ name: 'update_at' })
    updatedAt: Date;
}
