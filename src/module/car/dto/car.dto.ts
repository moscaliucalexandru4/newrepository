import { ApiProperty } from '@nestjs/swagger';

export class CarDto {
  @ApiProperty()
    id!: number;

  @ApiProperty({ example: 'BMW' })
    brand!: string;

  @ApiProperty({ example: 'X5' })
    model!: string;

  @ApiProperty({ example: 2005 })
    year!: number;

  @ApiProperty()
    created_at!: Date;

  @ApiProperty()
    updated_at!: Date;
}
