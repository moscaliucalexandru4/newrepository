import { ApiProperty } from '@nestjs/swagger';
import { Length } from 'class-validator';

export class CreateCarDto {
  @ApiProperty({ example: 'BMW', description: 'Car brand', type: String })
  @Length(3, 30, { message: 'Brand supposed to have more than 2 characters' })
    brand!: string;

  @ApiProperty({ example: 'X5', description: 'Car model', type: String })
  @Length(1, 30, { message: 'Model supposed to have minimum 1 character' })
    model!: string;

  @ApiProperty({ example: '2005', description: 'Car production year', type: String })
  @Length(4, 4, { message: 'Year supposed to have 4 characters' })
    year!: number;
}
