import {
  Body, Controller, Delete, Get, HttpCode, Param, Patch, Post, Query,
} from '@nestjs/common';
import {
  ApiBody, ApiOkResponse, ApiOperation, ApiResponse, ApiTags,
} from '@nestjs/swagger';

import { Pageable } from '../pagination/pageable.dto';

import { DriverService } from './driver.service';
import { CreateDriverDto } from './dto/create-driver.dto';
import { DriverDto } from './dto/driver.dto';
import { UpdateDriverDto } from './dto/update-driver.dto';

import type { Driver } from './driver-entity/driver.entity';
import type { PageResponse } from '../pagination/response.type';

@ApiTags('driver')
@Controller('drivers')
export class DriverController {
  constructor(
    private readonly driverService:DriverService,
  ) {
  }

  @Get()
  @ApiOperation({ summary: 'Endpoint to get all drivers' })
  @ApiOkResponse({
    description: 'Successful get all drivers',
    type: [DriverDto],
  })
  @ApiResponse({ status: 500, description: 'Failed to get all drivers.' })

  async findAll(@Query() filter:Pageable): Promise<PageResponse<Driver[]>> {
    return this.driverService.findAll(filter);
  }

  @Get(':id')
  @ApiOperation({ summary: 'Endpoint to get driver' })
  @ApiOkResponse({
    description: 'Successful get driver',
    type: DriverDto,
  })
  @ApiResponse({ status: 500, description: 'Failed to get driver.' })
  async findOne(@Param('id')id:number):Promise<Driver | null> {
    return this.driverService.findOne(id);
  }

  @Post()
  @ApiOperation({ summary: 'Endpoint to create a driver' })
  @ApiResponse({ status: 500, description: 'Failed to create the driver.' })
  @ApiOkResponse({
    description: 'The driver has been successfully created.',
    type: DriverDto,
  })
  @ApiBody({ type: CreateDriverDto })
  async create(@Body() input: CreateDriverDto):Promise<Driver> {
    return this.driverService.createDriver(input);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Endpoint to update a driver' })
  @ApiOkResponse({
    description: 'The driver has been successfully updated.',
    type: DriverDto,
  })
  @ApiResponse({ status: 500, description: 'Failed to update the driver.' })
  async update(@Param('id') id:number, @Body() input:UpdateDriverDto):Promise<Driver> {
    return this.driverService.update(id, input);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Endpoint to delete a driver' })
  @ApiOkResponse({
    description: 'The driver has been successfully deleted.',
    type: DriverDto,
  })
  @ApiResponse({ status: 500, description: 'Failed to delete the driver.' })
  @HttpCode(204)
  async delete(@Param('id') id:number):Promise<void> {
    await this.driverService.delete(id);
  }
}
