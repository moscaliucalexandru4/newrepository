import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { NotFound } from '../errors/not-found-exception';

import { Driver } from './driver-entity/driver.entity';

import type { CreateDriverDto } from './dto/create-driver.dto';
import type { UpdateDriverDto } from './dto/update-driver.dto';
import type { Pageable } from '../pagination/pageable.dto';
import type { PageResponse } from '../pagination/response.type';
import type { DeleteResult } from 'typeorm';

@Injectable()
export class DriverService {
  private readonly logger = new Logger(DriverService.name);

  constructor(
    @InjectRepository(Driver)
    private readonly repository: Repository<Driver>,
  ) {
  }

  async findAll(filter:Pageable)
    : Promise<PageResponse<Driver[]>> {
    this.logger.log('Hit the findAll router');
    const { page, limit } = filter;
    const [data, total] : [Driver[], number] = await this.repository.findAndCount({
      skip: filter.getSkip(),
      take: filter.limit,
    });
    return {
      data,
      page,
      limit,
      total,
    };
  }

  async findOne(id: number): Promise<Driver | null> {
    this.logger.log('Hit the findOne router');
    const driver: Driver | null = await this.repository.findOneBy({ id });
    if (!driver) {
      throw new NotFound();
    }
    return driver;
  }

  async createDriver(input: CreateDriverDto): Promise<Driver> {
    this.logger.log('Hit the create router');

    return this.repository.save(input);
  }

  async update(id: number, input: UpdateDriverDto): Promise<Driver> {
    this.logger.log('Hit the update router');

    const driver: Driver | null = await this.repository.findOneBy({ id });
    if (!driver) {
      throw new NotFound();
    }
    await this.repository.update(
      id,
      input,
    );
    return driver;
  }

  async delete(id: number): Promise<DeleteResult> {
    this.logger.log('Hit the delete router');
    const driver: Driver | null = await this.repository.findOneBy({ id });

    if (driver) {
      return this.repository.delete(driver.id);
    }
    throw new NotFound();
  }
}
