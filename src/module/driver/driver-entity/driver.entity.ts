import {
  Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn,
} from 'typeorm';

@Entity('driver')
export class Driver {
  constructor(
    id: number,
    firstName: string,
    lastName: string,
    birthDate: Date,
    driverLicenceIssuedAt: Date,
    driverLicenceExpiredAt: Date,
    createdAt: Date,
    updatedAt: Date,
  ) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthDate = birthDate;
    this.driverLicenceIssuedAt = driverLicenceIssuedAt;
    this.driverLicenceExpiredAt = driverLicenceExpiredAt;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  @PrimaryGeneratedColumn()
    id: number;

  @Column({ name: 'first_name', type: 'varchar', length: 30 })
    firstName: string;

  @Column({ name: 'last_name', type: 'varchar', length: 30 })
    lastName: string;

  @Column({ name: 'birth_date', type: 'date' })
    birthDate: Date;

  @Column({ name: 'driver_licence_issued_at', type: 'date' })
    driverLicenceIssuedAt: Date;

  @Column({ name: 'driver_licence_expired_at', type: 'date' })
    driverLicenceExpiredAt: Date;

  @CreateDateColumn()
    createdAt: Date;

  @UpdateDateColumn()
    updatedAt: Date;
}
