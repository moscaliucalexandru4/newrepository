import { ApiProperty } from '@nestjs/swagger';

export class DriverDto {
  @ApiProperty()
    id!: number;

  @ApiProperty({ example: 'James' })
    firstName!: string;

  @ApiProperty({ example: 'Smith' })
    lastName!: string;

  @ApiProperty({ example: '10-25-2000' })
    birthDate!: Date;

  @ApiProperty({ example: '11-28-2018' })
    driverLicenceIssuedAt!: Date;

  @ApiProperty({ example: '11-28-2028' })
    driverLicenceExpiredAt!: Date;

  @ApiProperty()
    created!: Date;

  @ApiProperty()
    updated!: Date;
}
