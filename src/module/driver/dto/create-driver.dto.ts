import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsDate, Length } from 'class-validator';

const date = 'Month-Day-Year';
export class CreateDriverDto {
  @ApiProperty({ example: 'James', description: 'Driver first name', type: String })
  @Length(3, 35, { message: 'Your first name length should be between 3 and 35' })
    firstName!:string;

  @ApiProperty({ example: 'Smith', description: 'Driver last name' })
  @Length(3, 35, { message: 'Your first name length should be between 3 and 35' })
    lastName!: string;

  @ApiProperty({ example: '10-25-2000', description: date })
  @IsDate()
  @Type(() => { return Date; })
    birthDate!: string;

  @ApiProperty({ example: '11-28-2018', description: date })
  @IsDate()
  @Type(() => { return Date; })
    driverLicenceIssuedAt!: string;

  @ApiProperty({ example: '11-28-2028', description: date })
  @IsDate()
  @Type(() => { return Date; })
    driverLicenceExpiredAt!: string;
}
