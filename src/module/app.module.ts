import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Car } from './car/car-entity/car-entity';
import { CarModule } from './car/car.module';
import { Driver } from './driver/driver-entity/driver.entity';
import { DriverModule } from './driver/driver.module';
import { EmployeeEntity } from './employee/employee-entity/employee.entity';
import { EmployeeModule } from './employee/employee.module';

@Module({
  imports: [
    CarModule, DriverModule, EmployeeModule,
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService:ConfigService) => {
        return {
          type: 'postgres',
          host: configService.get('DB_HOST'),
          port: +configService.get('DB_PORT'),
          username: configService.get('DB_USER'),
          password: configService.get('DB_PASSWORD'),
          database: configService.get('DB_DB'),
          entities: [Car, EmployeeEntity, Driver],
          synchronize: configService.get('APP_ENABLE'),
          logging: configService.get('APP_ENABLE'),
        };
      },

    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
}
